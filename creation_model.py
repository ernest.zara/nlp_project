import pandas as pd
import torch
from transformers import BertTokenizer, BertModel


data = pd.read_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_labels.csv',
)


data['comments'] = data['comments'].fillna('')
data['label'] = data['label'].fillna('')

# embeddings avec BERT
