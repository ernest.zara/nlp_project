import pandas as pd
import re


path_write = '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie'
# Charger le DataFrame depuis le fichier CSV
# Remplacez par le chemin correct de votre fichier CSV
data_sujet = pd.read_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/doctissimo_comments.csv')


# Fonction pour nettoyer et transformer les URLs
def nettoyer_url(url):

    # Extraire le dernier segment de l'URL
    last_segment = url.split('/')[-1]
    # Supprimer tout ce qui suit le dernier tiret (c'est-à-dire le numéro de sujet et l'extension)
    last_segment = re.sub(r'_\d+\.htm$', '', last_segment)
    # Remplacer les tirets par des espaces
    clean_segment = last_segment.replace('-', ' ')
    # Supprimer les mentions, hashtags, et caractères spéciaux
    clean_segment = re.sub(r'[^a-zA-Z\s]', '', clean_segment)

    return clean_segment


# Fonction pour nettoyer les commentaires
def nettoyer_comments(comments):
    if pd.isna(comments):
        return ''

    # Supprimer les URLs
    comments = re.sub(r'http\S+', '', comments)
    # Supprimer les mentions (@username)
    comments = re.sub(r'@\w+', '', comments)
    # Supprimer les hashtags (#hashtag)
    comments = re.sub(r'#\w+', '', comments)
    # Supprimer les ponctuations et les caractères spéciaux
    comments = re.sub(r'[^\w\s]', '', comments)

    return comments


# Appliquer la fonction d'extraction sur la colonne 'url'


data_sujet['label'] = data_sujet['url'].apply(nettoyer_url)
data_sujet['comments'] = data_sujet['body'].apply(nettoyer_comments)

data_sujet_cleannes = data_sujet[['label', 'comments']]

# Enregistrer le DataFrame mis à jour dans un nouveau fichier CSV
data_sujet_cleannes.to_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_cleanned.csv',
    index=False)

data_sujet.to_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_base.csv',
    index=False)

print("Data updated and saved to data_doctissimo_cleanned.csv")
