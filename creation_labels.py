import pandas as pd
import nltk
from nltk.corpus import stopwords

data = pd.read_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_tokenized.csv')

# Liste des maladies
identified_diseases = [
    "cholestas", "toxo", "rubeole", "rubeole-toxoplasme", "anemie-hemolytique", "anesthesie-generale",
    "listeria", "listeriose", "eclampsie", "autisme", "candida-albicans", "chlamydia",
    "cholestase", "cytomegalovirus", "parvorirus", "ponderal rhesus", "toxoplasmose", "rhesus",
    "fibromyalgie", "gardenella vaginalis", "gardenella", "gardnella", "hydrothyroide", "hydramnios", "infection urinaire",
    "infection vaginale", "infection vaginal", "infection bacterienne", "infection bacterie",
    "cagardnella vaginal", "infection", "vaginal", "vaginale", "vaginalis"

]

# Fonction pour filtrer les mots dans les commentaires


def create_labels_from_comments(comment, disease_list):
    words = nltk.word_tokenize(comment.lower())
    found_labels = [word for word in words if word in disease_list]
    return ' '.join(found_labels)


# Pour les labels vides, rechercher dans les commentaires
def update_label_from_comments(row, disease_list):
    if row['label'] == '':
        return create_labels_from_comments(row['comments'], disease_list)
    return row['label']


data['label'] = data['label'].fillna('')
data['comments'] = data['comments'].fillna('')

data['label'] = data['label'].apply(
    lambda x: create_labels_from_comments(x, identified_diseases))

data['label'] = data.apply(
    lambda row: update_label_from_comments(row, identified_diseases), axis=1)


# Enregistrer le DataFrame mis à jour dans un nouveau fichier CSV
data.to_csv('/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_labels.csv', index=False)

print("Labels found in comments and saved to data_doctissimo_labels.csv")
