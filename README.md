# grossesse et pathologies infectieuses



## Définition du problème

Analyser les échanges entre les mamans sur les réseaux sociaux ou les blogs de grossesse afin de :
- Identifier les mentions de maladies infectieuses.
- Identifier les effets indésirables potentiels sur le fœtus ou le bébé.

## Collecte de données

- [ ] [Sources de données :]  https://forum.doctissimo.fr/grossesse-bebe/Maladies-infectieuses-et-grossesse
	
- Méthode de collecte : Utilisation de *BeautifulSoup* et *Python* pour scraper les commentaires 

## Prétraitement des données 

- Nettoyage des données :
	- Suppression des URLs, mentions, hashtags, ponctuations et caractères spéciaux.
	- La tokenization : consiste à diviser un texte en unités plus petites appelées tokens (souvent des mots ou des sous-mots).
	- Lowercasing : convertit tous les caractères d'un texte en minuscules 
	- La suppression des stop words : Les stop words sont des mots courants (comme "et", "le", "la", "de" en français) qui sont souvent filtrés car ils n'apportent pas beaucoup de sens en termes de contenu.

	- La lemmatisation et la stemmatisation : réduit les mots à leur forme canonique (par exemple, "marchant" --> "marcher"), tandis que la stemming coupe les suffixes pour obtenir le radical du mot (par exemple, "marchant" --> "marche").

## Techniques et modèles

- Embeddings : 
	- Utilisation de BERT (via la bibliothèque transformers de Hugging Face) pour obtenir des représentations contextuelles des textes.
	
	- Classification : (*Tâches de NLP*)

		- Détection des mentions de maladies infectieuses et des effets indésirables.
		- Reconnaissance d'entités nommées (NER) : Identifier les noms des maladies et les symptômes.
		- Analyse de sentiments : Évaluer le ton des discussions (anxiété, inquiétude, etc.).

## Modèles et entraînement

- Classification :
	- Modèle : Utilisation de BERT pour la classification de texte.
	- Entraînement : Fine-tuning de BERT sur notre corpus de données collectées.



## Test and Deploy
En cours... 

## Auteurs : 
- Sepe W. 
- Ernest R. 

## License
For open source projects, say how it is licensed.

## Project status
En cours... 
