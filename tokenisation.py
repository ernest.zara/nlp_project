import pandas as pd
import re
import nltk
import spacy
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords


# Charger le DataFrame depuis le fichier CSV
data_sujet = pd.read_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_cleanned.csv')

# Fonction de tokenization, lowercasing et suppression des stop words


def teken_commentaire(comments):
    if pd.isna(comments) or comments.strip() == '':
        return ''

    # tokenisation
    tokens = word_tokenize(comments)
    # miniscule
    tokens = [token.lower() for token in tokens]
    # suppression des stop words
    tokens = [token for token in tokens if token.isalpha()
              and token not in stop_words]

    cleaned_words = []
    for token in tokens:
        # via regex :suppr les lettres avant l'apostrophe et l'apostrophe elle-même
        cleaned_word = re.sub(r"\b\w+'", '', token)
        if cleaned_word:  # S'assurer que le mot n'est pas vide après nettoyage
            cleaned_words.append(cleaned_word)

    #tokens = [re.sub(r"\b\w+\'", '', token) for token in tokens]

    # Initialiser Spacy pour lemmatisation
    #nlp = spacy.load('fr_core_news_sm')
    #doc = nlp(' '.join(tokens))
    #lemmas = [token.lemma_ for token in doc]

    return cleaned_words


stop_words = set(stopwords.words('french'))
data_sujet['comments'] = data_sujet['comments'].apply(
    lambda x: ' '.join(teken_commentaire(x)))

data_sujet['label'] = data_sujet['label'].apply(
    lambda x: ' '.join(teken_commentaire(x)))

#data_sujet['url'] = data_sujet['url'].apply(teken_commentaire)
#data_sujet['body'] = data_sujet['body'].apply(teken_commentaire)

# Enregistrer le DataFrame mis à jour dans un nouveau fichier CSV
data_sujet.to_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_tokenized.csv', index=False)

print("Data preprocessed and saved to data_doctissimo_tokenized.csv")
