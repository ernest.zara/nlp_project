import pandas as pd
import torch
from transformers import BertTokenizer, BertModel

# Charger le DataFrame depuis le fichier CSV
data = pd.read_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_preprocessed.csv')

# Initialiser le tokenizer et le modèle BERT
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased')

# Fonction pour obtenir les embeddings BERT d'un texte


def get_bert_embeddings(text):
    inputs = tokenizer(text, return_tensors='pt',
                       truncation=True, padding=True,
                       max_length=512)
    with torch.no_grad():
        outputs = model(**inputs)
    # Utiliser les embeddings du [CLS] token pour représenter le texte
    embeddings = outputs.last_hidden_state[:, 0, :].cpu().numpy()
    return embeddings[0]


# Appliquer les embeddings BERT sur les commentaires
data['comments'] = data['comments'].astype(str)
data['embeddings'] = data['comments'].apply(lambda x: get_bert_embeddings(x))

data.to_csv('/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_embeddings.csv',
            index=False)
print("Embeddings generated and saved to data_doctissimo_embeddings.csv")
