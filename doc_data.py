from urllib.request import urlopen
from bs4 import BeautifulSoup
from urllib.error import HTTPError
import pandas as pd

# Génération des liens des pages
links_page = ["https://forum.doctissimo.fr/grossesse-bebe/Maladies-infectieuses-et-grossesse/liste_sujet-" +
              str(i) + ".htm" for i in range(10)]


def get_post_links(link):
    try:
        html = urlopen(link)
        soup = BeautifulSoup(html, "html.parser")
    except HTTPError as e:
        print("URL not valid:", link)
        return set()

    temp = soup.find_all('tr', class_='sujet')
    links = set()
    for post in temp:
        a_tag = post.find('a')
        if a_tag and 'href' in a_tag.attrs:
            links.add(a_tag['href'])
    return links


def read_html(link):
    try:
        html = urlopen(link)
        return BeautifulSoup(html, "html.parser")
    except HTTPError as e:
        print("URL not valid:", link)
        return None


def get_comments(link):
    soup = read_html(link)
    if soup:
        comments = []
        posts = soup.find_all(class_="md-post__content")
        for post in posts:
            header = post.find_previous("header")
            metadata = {}
            if header:
                for meta in header.find_all("span"):
                    metadata[meta.get('class')[0]] = meta.get_text().strip()
            text = post.find(itemprop="text").get_text().strip()
            body = text.replace("\n", "")
            body = body.replace("&#034;", '"')
            comments.append({'url': link, 'metadata': metadata, 'body': body})
        return comments
    return []


def scrape_forum():
    all_post_links = set()
    for page_link in links_page:
        all_post_links.update(get_post_links(page_link))

    print("Total threads found:", len(all_post_links))

    all_comments = []
    for post_link in all_post_links:
        full_link = "https://forum.doctissimo.fr" + \
            post_link if not post_link.startswith("https://") else post_link
        print("Scraping comments from:", full_link)
        comments = get_comments(full_link)
        all_comments.extend(comments)

    return all_comments


# Lancer le scraping
comments_data = scrape_forum()

# Créer un DataFrame à partir des commentaires
df = pd.DataFrame(comments_data)

# Écrire le DataFrame dans un fichier CSV
df.to_csv('doctissimo_comments.csv', index=False)

print("Data saved to doctissimo_comments.csv")
