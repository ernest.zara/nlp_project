import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
import numpy as np


d = pd.read_csv(
    '/Users/zarasoa/datascience/master2/nlp/grossesse_pathologie/grossesse_pathologie/data_doctissimo_embeddings.csv')

db = d[['embeddings']]
# Convertir les embeddings de chaîne de caractères à numpy array
db['embeddings'] = db['embeddings'].apply(
    lambda x: np.fromstring(x.strip('[]'), sep=' '))
db['label'] = ''

# Préparer les caractéristiques (embeddings) et les étiquettes (à définir dans votre dataset)
X = np.stack(db['embeddings'].values)
y = db['label']  # Assurez-vous d'avoir une colonne 'label' pour les classes

# Diviser les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42)

# Training du modèle de classification
classifier = LogisticRegression(max_iter=100)
classifier.fit(X_train, y_train)

# Prédire sur l'ensemble de test
y_pred = classifier.predict(X_test)

# Afficher les résultats
print(classification_report(y_test, y_pred))
